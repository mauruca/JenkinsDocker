# Jenkins Docker


## S3
In order to use a S3 volume on any server.

- Add a IAM user with programatic access and attach existing policie S3fullaccess.
- Save user credentials access key id and secret access key.
- Create a config file at http://rexrayconfig.codedellemc.com/
- save the config file in /etc/rexray
- start the service
- create a volume
```bash
$ rexray volume create xk97fa7cppnuvkch
```