#!/bin/bash
# Copyright (C) 2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

echo "=================================="
echo "       Building Jenkins           "
echo "=================================="

if [ -z "$1" ]
  then
    echo "./build.sh <buildtag> <optional dockerfile>"
    exit 1
fi

ver="$1-alpine"

arq=""

if [ "$3" != "" ]
  then
    arq="-f $3"
fi

docker build $arq -t ultimaratioregis/jenkins:$ver .
docker build $arq -t ultimaratioregis/jenkins:alpine .
docker images ultimaratioregis/jenkins

exit 0