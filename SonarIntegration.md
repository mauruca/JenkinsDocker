# Sonarqube integration in swarm mode

This is a how to integrate Sonaqube with you Jenkins in swarm mode.

## Install a Sonarqube
Follow the instructions [here](https://github.com/maurucao/sonarqube) to install a sonar in your cluster.

## Jenkins setup
1. Install the plugin
Install the plugin following the instructions [here](http://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner+for+Jenkins).

2. Setup a SonarQube Scanner
Select Manage Jenkins > Global Tool Configuration > SonarQube Scanner > Add SonarQube Scanner(button). Type any name, select the option to install automatically and the latest scan version. Save the configuration.

3. Setup Sonarqube server
Select Manage Jenkins > Configure System > SonarQube servers > Add SonarQube(button). Type any name, add server url that is http://anynodeip:port(:9000 normally) and select a server version according to your server. Save the configuration.  
We can use any node ip because the swarm built in load balance will send us to correct node.

4. Add a scan build step
In your project config, add a build step **Execute SonarQube Scanner**. In Analisys properties field insert a project configuration parameters as below example and save the configuration.
```
# required metadata
sonar.projectKey=hereditas:portal:HEAD
sonar.projectName=MyProject
sonar.projectVersion=2.0

# path to source directories (required)
sonar.sources=web/django

# Uncomment this line to analyse a project which is not a java project.
# The value of the property must be the key of the language.
sonar.language=py
```

5. Build the project.
