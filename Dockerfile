FROM jenkins:alpine

MAINTAINER mauruca <mpinheiro@pobox.com>

ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false -Dhudson.footerURL=http://www.ur2.com.br"

COPY plugins.list /usr/share/jenkins/ref/plugins.txt
COPY custom.groovy /usr/share/jenkins/ref/init.groovy.d/custom.groovy

RUN /usr/local/bin/install-plugins.sh $(cat /usr/share/jenkins/ref/plugins.txt)

USER root
RUN chown jenkins:jenkins /var/jenkins_home
USER ${user}