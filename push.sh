#!/bin/bash
# Copyright (C) 2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

echo "=================================="
echo "       Pushing Jenkins           "
echo "=================================="

if [ -z "$1" ]
  then
    echo "./build.sh <buildtag> <optional dockerfile>"
    exit 1
fi

ver="$1-alpine"

docker push ultimaratioregis/jenkins:$ver
docker push ultimaratioregis/jenkins:alpine

exit 0