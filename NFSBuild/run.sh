#!/bin/sh
# 2017 Mauricio Costa Pinheiro

containername="jkbuilderhereditas"
running=$(docker ps -a -f name=$containername | grep $containername)
if [ ! -z "$running" ]; then
  docker stop $containername
  docker rm $containername
fi
if [ ! -d "home" ]; then
  mkdir home
fi
docker run -d --name $containername -p 8888:8080 -p 50000:50000 -v $(pwd)/home:/var/jenkins_home jenkins:alpine
sleep 10
docker logs $containername
