# NFS setup

This is a mini how to setup a NFS server and client for debian distributions.  
This config uses /etc/idmapd.conf to control access using nobody user

## NFS Server

1. Install the server modules
sudo apt-get install nfs-kernel-server

2. Create the share folder
sudo mkdir /var/nfs
sudo mkdir /var/nfs/*pathtoshare*

3. Change the permission to share folder
sudo chown nobody:nogroup /var/nfs/*pathtoshare*

4. Add the share to /etc/exports file. Change the address 192.168.1.0 for your network address keeping the end .0/24 to reach all nodes in your network.
echo "/var/nfs/pathtoshare 192.168.1.0/24(rw,sync,no_subtree_check)" | sudo tee -a /etc/exports

5. Export the shares
sudo exportfs -ra

6. Restart the NFS service
sudo service nfs-kernel-server restart

## NFS clients

1. Install the client modules
apt-get install nfs-common

2. Create a local path to share /var/*pathtoshare*
sudo mkdir /var/*pathtoshare*

3. Add the mount to /etc/fstab
echo "*setnfsserverip*:/var/nfs/*pathtoshare* /var/*pathtoshare* nfs auto,noatime,nolock,bg,nfsvers=4,intr,tcp,actimeo=1800 0 0" | sudo tee -a /etc/fstab

4. Mount the share
sudo mount -a

5. Check the mount
sudo df -h
