#!/bin/sh
# MIT License - Copyright (C) 2016 Mauricio Costa Pinheiro.

until $(curl --output /dev/null --silent --head --fail http://$1); do
    printf '.'
    sleep 1
done

