#!/bin/bash
sudo apt-get install nfs-common -y

sudo mkdir -p /var/jenkins_home
#sudo mount -t nfs -o proto=tcp,port=2049 10.11.1.150:/var/nfs/jenkins_home /var/jenkins_home/

echo "$1:/var/nfs/jenkins_home /var/jenkins_home nfs auto,noatime,nolock,bg,nfsvers=4,intr,tcp,actimeo=1800 0 0" | sudo tee -a /etc/fstab
sudo mount --all
