# S3 Jenkins in swarm mode

## Run command
Jenkins user and pass will be informed by docker secrets jenkins-user and jenkins-pass

    echo "user" | docker secret create jenkins-user -

    echo "password" | docker secret create jenkins-pass -

Change deploy configuration in the file jenkins.yml and run the command:

    docker stack deploy -c jenkins.yml jenkins

## Build
Set a list of plugins in plugins.list in the format $JENKINS_PLUGIN:$JENKINS_PLUGIN_VER.

# NFS Jenkins in swarm mode

Create a NFS server to share config, data and extensions. check [this mini how to](https://github.com/maurucao/jenkins/blob/master/NFS.md).

This config was made using the [Jenkins docker image](https://hub.docker.com/_/jenkins/).

First you must create a NFS to share the common server files. This will avoid reconfiguration of new containers. As I am using just one container instance, I created one share called /var/nfs/jenkins_home. If you plan to have more you can add a subfolder to each server /var/nfs/jenkins_home/S1 en sync the content.

Add to each node in the cluster, that will run the Jenkins container, a line in the /etc/fstab and mount the share(sudo mount -a). The line below is an example:

    **yourNFSserver**:/var/nfs/jenkins_home /var/jenkins_home nfs auto,noatime,nolock,bg,nfsvers=4,intr,tcp,actimeo=1800 0 0

You can use also [this](https://github.com/maurucao/jenkins/blob/master/helpers/mountNFSJenkinsClientNode.sh) helper script.

To run the service in you swarm mode cluster run the command below in a manager node.  
Pay attention to the volumes.

docker service create \  
    --name jenkins -p 9000:9000 -p9092:9092 \  
    --mount type=bind,src=/var/jenkins_home,dst=/var/jenkins_home \  
    --replicas **1** jenkins:alpine
  
To integrate Jenkins and Sonarqube try [these](https://github.com/maurucao/jenkins/blob/master/SonarIntegration.md) instructions, but first configure the Sonarqube in swarm [here](https://github.com/maurucao/sonarqube).
